
import * as axios from 'axios';

//const BASE_URL = 'http://localhost:8080';

function upload(formData) {
    return axios({
        method: 'POST',
        url: 'https://us-central1-emotion-lt-functions.cloudfunctions.net/newTalentFiles',
        headers: {
            'Content-Type': 'multipart/form-data' 
        },
        data: formData
    });
    // return axios.post('http://localhost:5000/emotion-lt-functions/us-central1/newTalentFiles', formData);
        // // get data
        // .then(x => x.data)
        // // add url field
        // .then(x => x.map(img => Object.assign({},
        //     img, { url: `${BASE_URL}/photos/${img.id}` })));
}

export { upload }