import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueAnalytics from 'vue-analytics'
import "./vee-validate";

import routes from './routes'

Vue.config.productionTip = false;

Vue.use(VueRouter);

Vue.prototype.$careerEmpty = false;

const router = new VueRouter({
    mode:'history',
    scrollBehavior(to, from, savedPosition){
        if (to.hash) {
            return { selector: to.hash}
        } else if (savedPosition) {
            return savedPosition;
        } else {
            return {x:0, y:0}
        }
    },
    routes
});

Vue.use(VueAnalytics, {
    id: 'UA-24781863-1',
    // disabled: false,
    // debug: {
    //     enabled: true,
    //     trace: true,
    //     sendHitTask: true
    // },
    router
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
