import colorPalette from "./colorPalette";

const {
    COLOR_LANDSCAPE,
    COLOR_BORDERS,
    COLOR_WATER,
    COLOR_POINT_FILL,
    COLOR_SELECTED_POINT
} = colorPalette;

const COLORS = {
    BORDERS: COLOR_BORDERS,
    LANDSCAPE: COLOR_LANDSCAPE,
    POINT: COLOR_SELECTED_POINT,
    POINT_FILL: COLOR_POINT_FILL,
    WATER: COLOR_WATER
};

const POINT_MARKER_ICON_CONFIG = {
    //path: "M 12,2 C 8.1340068,2 5,5.1340068 5,9 c 0,5.25 7,13 7,13 0,0 7,-7.75 7,-13 0,-3.8659932 -3.134007,-7 -7,-7 z",
    //origin: { x: 0, y: 0 },
    path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
    strokeOpacity: 1,
    strokeWeight: 3,
    strokeColor: COLORS.POINT,
    fillColor: COLORS.POINT_FILL,
    fillOpacity: 0,
    scale: 1,
   // url: 'http://maps.google.com/mapfiles/ms/icons/purple-dot.png'
};

const mapSettings = {
    clickableIcons: false,
    streetViewControl: false,
    panControlOptions: false,
    gestureHandling: "cooperative",
    backgroundColor: COLORS.LANDSCAPE,
    mapTypeControl: false,
    zoomControlOptions: {
        style: "SMALL"
    },
    zoom: 15,
    minZoom: 2,
    maxZoom: 20,
    styles: [
        {
            featureType: "landscape",
            stylers: [
                /*{ hue: COLORS.LANDSCAPE },
                { saturation: 50.2 },
                { lightness: -34.8 },
                { gamma: 1 }*/
                {color: '#200C3E'}
            ]
        },
        {
            featureType: "poi",
            stylers: [{ visibility: "off" }]
        },
        {
            featureType: "road",
            stylers: [
                 // { hue: COLORS.LANDSCAPE },
                 // { saturation: 72.4 },
                 // { lightness: -32.6 },
                 // { gamma: 1 },
                {color: '#441587'}
            ]
        },
        {
            featureType: "road",
            elementType: "labels.text.fill",
            stylers: [{ color: '#6920d0' }]
        },
        {
            featureType: "transit.line",
            elementType: "labels.text.fill",
            stylers: [{ color: '#200C3E' }]
        },
        {
            featureType: "transit.line",
            elementType: "geometry.fill",
            stylers: [{ color: '#531aa4' }]
        },
        {
            featureType: "transit.station",
            stylers: [{ visibility: "off" }]
        },

        {
            featureType: "administrative.province",
            stylers: [{ visibility: "off" }]
        },
        {
            featureType: "administrative.locality",
            stylers: [{ visibility: "off" }]
        },
        {
            featureType: "administrative.province",
            stylers: [{ visibility: "off" }]
        },
        {
            featureType: "administrative.land_parcel",
            stylers: [{ visibility: "off" }]
        },
        {
            featureType: "administrative.neighborhood",
            stylers: [{ visibility: "off" }]
        },
        {
            featureType: "administrative.country",
            elementType: "geometry.stroke",
            stylers: [{ visibility: "on" }, { color: COLORS.BORDERS }]
        },
        {
            featureType: "administrative",
            elementType: "labels",
            stylers: [{ visibility: "off" }]
        },
        {
            featureType: "water",
            stylers: [
                /*{ hue: COLORS.WATER },*/
                /*{ saturation: -63.2 },*/
                /*{ lightness: 38 },*/
                /*{ gamma: 1 }*/
                {color: '#441587'}
            ]
        }
    ]
};

export { mapSettings, POINT_MARKER_ICON_CONFIG };
