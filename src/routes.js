
import firstPage from "@/components/Pages/firstPage";
import aboutPage from "@/components/Pages/aboutUsPage";
import career from "@/components/Pages/career";
import contactUsPage from "@/components/Pages/contactUsPage";
import privacyPolicy from "@/components/Pages/privacyPolicy";
import pageNotFound from "@/components/pageNotFound";

	const routes = [
		{path: '/', component: firstPage},
		{path: '/about-us', component: aboutPage},
		{path: '/career', component: career},
		{path: '/contact-us', component: contactUsPage},
		{path: '/privacy-policy', component: privacyPolicy},
		{path: '/404', component: pageNotFound},
		{path: '*', redirect: '/404'},
	];

 export default routes;



