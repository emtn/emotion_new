import { extend, localize } from "vee-validate";
import { configure } from 'vee-validate';
import { min, required, email, numeric } from "vee-validate/dist/rules";
import en from "vee-validate/dist/locale/en.json";


// Install rules
extend('required', {
    ...required,
    message: '{_field_} is required'});
extend("min", min);
extend("email", {
    ...email,
    message: 'E-mail is not in correct format'});
extend("numeric", {
    ...numeric,
    message: 'Phone may only contain numeric characters'});

// Install messages
localize({
    en
});
configure({
    classes: {
        invalid: 'invalid', // one class
    }
});
