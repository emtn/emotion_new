const Joi = require('@hapi/joi');
const nodemailer = require('nodemailer');

const admin = require('firebase-admin');

var serviceAccount = require("./emotion-lt-functions-firebase-adminsdk-yo5uf-f83665e252.json");

module.exports = async function newTalent(request, response) {
    if (request.method !== 'POST') {
        response.status(405).json({
            error: 'invalid method'
        });
    }
    
    // Schema used to validate request body (@hapi/joi library)
    const schema = Joi.object({
        name: Joi.string().trim().required(),
        email: Joi.string().trim().email().required(),
        message: Joi.string().trim().allow(''),
        files: Joi.array().items(Joi.string()).min(1).required()
    });

    const { error, value: input } = schema.validate(request.body, {
        abortEarly: false
    });

    if (error)
        return response.status(400).json(error);

    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://emotion-lt-functions.firebaseio.com",
        storageBucket: 'gs://emotion-lt-functions.appspot.com'
    });

    // In case you want to change the email, make sure that new email accepts 'less secure apps' in google account security page
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'newtalent@emotion.lt',
            pass: 'Pass4edita'
        }
    });

    const mailOptions = {
        to: 'newtalent@emotion.lt', // List of receivers
        subject: 'New Talent - ' + input.name, // Subject line
        // Plain text/html body, standard HTML tags can be used to structurize email body
        html: `<b>Name: </b> ${input.name} <br>
                <b>Email: </b> ${input.email} <br>
                <b>Message: </b> ${input.message}`,
        attachments: []
    };

    // Add required files to email as attachments
    for (const file of input.files) {
        mailOptions.attachments.push({
            filename: file,
            content: admin.storage().bucket().file(file).createReadStream()
        });
    }

    await transporter.sendMail(mailOptions);

    // Remove sent files from the storage once email was sent
    for (const file of input.files)
        await admin.storage().bucket().file(file).delete();

    await admin.app().delete();
    response.status(200).send();
}