const functions = require('firebase-functions');

const cors = require('cors')({ origin: true });

const newTalent = require('./new-talent');
const newTalentFull = require('./new-talent-full');
const newTalentFiles = require('./new-talent-files');
const notificationSubscribe = require('./notification-subscribe');

exports.newTalent = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        await newTalent(request, response); // Accepts form and sends email
    });
});

exports.newTalentFull = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        await newTalentFull(request, response); // Accepts form and sends email
    });
});

exports.newTalentFiles = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        await newTalentFiles(request, response); // Accepts new files and stores them temporary, to use once form were filled
    });
});

exports.notificationSubscribe = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        await notificationSubscribe(request, response); // Accepts form and sends email
    });
});