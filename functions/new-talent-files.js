const crypto = require("crypto");
const path = require('path');
const os = require('os');
const fs = require('fs');

const admin = require('firebase-admin');

const BusBoy = require('busboy');

var serviceAccount = require("./emotion-lt-functions-firebase-adminsdk-yo5uf-f83665e252.json");

module.exports = async function newTalentFiles(request, response) {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://emotion-lt-functions.firebaseio.com",
        storageBucket: 'gs://emotion-lt-functions.appspot.com'
    });

    let uploadedFile;

    const busboy = new BusBoy({ headers: request.headers });

    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
        // if (mimetype !== 'image/jpeg' && mimetype !== 'image/png')
        //     return res.status(400).json({ error: 'Wrong file type submitted' });

        const fileName = regenerateFilename(filename);
        const filePath = path.join(os.tmpdir(), fileName);

        uploadedFile = {
            path: filePath,
            name: fileName,
            mimetype: mimetype
        };

        // Writes file to temporary location (On GCP it would write directly into memory)
        file.pipe(fs.createWriteStream(uploadedFile.path));
    });

    busboy.on('finish', async () => {
        if (!uploadedFile) {
            await admin.app().delete();
            return response.status(200).send();
        }

        await admin.storage().bucket().upload(uploadedFile.path, {
            resumable: false,
            metadata: {
                metadata: {
                    contentType: uploadedFile.mimetype
                }
            }
        });

        await admin.app().delete();

        response.status(200).json({
            filename: uploadedFile.name // We send back generated filename, later on, client provide it to us so we know what files assign to email
        });
    });

    busboy.end(request.rawBody);
}

function regenerateFilename(filename) {
    const fileName = crypto.randomBytes(16).toString("hex");
    const fileExt = filename.split('.')[filename.split('.').length - 1];

    return fileName + '.' + fileExt;
}