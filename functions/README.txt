To work with functions first you must run 'npm install -g firebase-tools' command to install Firebase CLI tools.
Then 'npm install' must be called, so it can install all requires dependencies.

Then you must authenticate to firebase so you can deploy them.
To do that first you need to have access to GCP project and then run 'firebase login' to authenticate.
Once this is done you can run functions locally and deploy them.

To test functions locally use 'npm run serve', this will start emulation of the functions on local machine.
While emulated, functions will be automaticallly refreshed once code is changed (No need to call 'npm run serve' for each code change).

To deploy functions use 'npm run deploy'.

All commands must be called within this folder.

Functions:
newTalentFiles - used to upload files to Firebase Storage. Each file will have randomly generated filename and lifespan of a one day (any file will be deleted automatically once it was stored for 24 hours).
newTalent - used to send email.