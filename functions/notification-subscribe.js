const Joi = require('@hapi/joi');
const nodemailer = require('nodemailer');

const admin = require('firebase-admin');

var serviceAccount = require("./emotion-lt-functions-firebase-adminsdk-yo5uf-f83665e252.json");

module.exports = async function notificationSubscribe(request, response) {
    if (request.method !== 'POST') {
        response.status(405).json({
            error: 'invalid method'
        });
    }

    // Schema used to validate request body (@hapi/joi library)
    const schema = Joi.object({
        email: Joi.string().trim().email().required(),
    });

    const { error, value: input } = schema.validate(request.body, {
        abortEarly: false
    });

    if (error)
        return response.status(400).json(error);

    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://emotion-lt-functions.firebaseio.com",
        storageBucket: 'gs://emotion-lt-functions.appspot.com'
    });

    // In case you want to change the email, make sure that new email accepts 'less secure apps' in google account security page
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'newtalent@emotion.lt',
            pass: 'Pass4edita'
        }
    });

    const mailOptions = {
        to: 'newtalent@emotion.lt', // List of receivers
        subject: 'Subscription to notify ', // Subject line
        // Plain text/html body, standard HTML tags can be used to structurize email body
        html: `I would like to get notified when new position opens.<br>
               <b>Email: </b> ${input.email} <br>`
    };

    await transporter.sendMail(mailOptions);

    await admin.app().delete();
    response.status(200).send();
};